#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/mman.h>
#include "fluke2.h"

#define MAXCLIENTS 5

// defined in fluke2cmd.c
extern struct fluke_command fcmd[];
// defined in misc.c
extern struct error_record errorlog;
extern struct servo_control servoctrl[5];
// defined in fluke2.c
extern char *firmwarebuf;
extern struct camera_window window_list[NUM_WINDOWS];
extern unsigned char *rle_buf;
extern unsigned char scribbler_orientation;

struct camera_control camctrl;
struct serial_control serctrl;

static int interrupted;

static void fluke2_server_sigint(int num)
{
 interrupted = 1;
}

main()
{
 struct pollfd fds[MAXCLIENTS+1];
 unsigned char port_rfcomm;
 int listenfd, ret, nfds, myclient, size, i, j;
 int command_rx_len;
 sdp_session_t *sdp_session;
 struct rfcomm_client btclient[MAXCLIENTS];
 unsigned int fluke2_rand_uuid[] = { 0x8302, 0x603d, 0xca37, 0x92e8 };
 unsigned char cmd;
 unsigned char *mmap_ptr;

 // initialize
 errorlog.pos = 0;
 errorlog.size = 0;
 errorlog.header = (char *)malloc(ERRORLOGSIZE + 20);
 if(!errorlog.header) {
  printf("ERROR: main initialization failed allocating errorlog memory\n");
  exit(1);
 }
 // First two bytes of errorlog are 2 byte header
 errorlog.log = errorlog.header + 2;
 memset(errorlog.header, 0, ERRORLOGSIZE + 2);

 flukelog("INFO: server starting\n");

 // load old connection keys from non-volatile flash data partition
 //   This allows reconnection without repairing after a power-down
 // But this doesn't work without restarting bluetoothd
 //   Run from bluekeyload in rd.d startup scripts instead
 // bluez_linkkeys_load();

 for(i=0;i<MAXCLIENTS;i++) {
  memset(&btclient[i], 0, sizeof(struct rfcomm_client));
  btclient[i].rx_buf = (char *)malloc(BTRXBUFSIZE+10);
  if(!btclient[i].rx_buf) {
   flukelog("ERROR: main initialization failed allocating btclient RX memory\n");
   exit(1);
  }
  btclient[i].tx_buf = (char *)malloc(BTTXBUFSIZE+10);
  if(!btclient[i].tx_buf) {
   flukelog("ERROR: main initialization failed allocating btclient TX memory\n");
   exit(1);
  }
 }

 for(i=0;i<NUM_WINDOWS;i++) {
  window_list[i].xlow = 0;
  window_list[i].xhigh = 0;
  window_list[i].ylow = 0;
  window_list[i].yhigh = 0;
  window_list[i].xstep = 1;
  window_list[i].ystep = 1;
 }

 firmwarebuf = NULL;

 camctrl.fd = 0;
 camctrl.command = 0;
 camctrl.client = NULL;
 camctrl.colorflag = 1;
 camctrl.dojpeg = 1;
 camctrl.jpegquality = JPEG_QUALITY_GOOD;
 camctrl.imagewidth = 1280;
 camctrl.imageheight = 800;

 serctrl.fd = 0;
 serctrl.command = 0;
 serctrl.client = NULL;
 serctrl.rx_len = 0;
 serctrl.rx_cnt = 0;
 serctrl.scribbler_version = 0;

 camctrl.image = (char *)malloc((camctrl.imagewidth * camctrl.imageheight) * 3 + 20);
 if(!camctrl.image) {
  flukelog("ERROR: main initialization failed allocating image_rgb memory\n");
  exit(1);
 }

 // assume jpeg compressed max size is 1/3 full size - is this enough space ?
 camctrl.jpegmyro = (char *)malloc((camctrl.imagewidth * camctrl.imageheight) + 20);
 if(!camctrl.jpegmyro) {
  flukelog("ERROR: main initialization failed allocating jpeg memory\n");
  exit(1);
 }
 // myro jpeg format has an extra 2 byte header
 camctrl.jpeg = camctrl.jpegmyro + 2;

 rle_buf = (char *)malloc(RLEBUFSIZE + 20);
 if(!rle_buf) {
  flukelog("ERROR: main initialization failed allocating rle_buf memory\n");
  exit(1);
 }

 if(gpio_export(GPIO_IRIN) ||
    gpio_export(GPIO_CAM_SCL) ||
    gpio_set_dir(GPIO_CAM_SCL, GPIOOUTPUT) ||
    gpio_export(GPIO_CAM_SDA) ||
    gpio_export(GPIO_BRIGHT_LED) ||
    gpio_set_dir(GPIO_BRIGHT_LED, GPIOOUTPUT)) {
  flukelog("ERROR: main initialization gpio export failed - group 1\n");
  exit(1);
 }

 // GPIO_IROUT is active low and must be high to stop IR transmissions
 if(gpio_export(GPIO_IROUT) ||
    gpio_set_dir(GPIO_IROUT, GPIOOUTPUT) ||
    gpio_set_value(GPIO_IROUT, 1)) {
  flukelog("ERROR: main initialization gpio export failed - GPIO_IROUT\n");
  exit(1);
 }

 // GPIO_SCRIB_RST (DTR pin) must be high to enable the scribbler's serial port
 //  the scribbler2 uses its DTR input as the pull down for its TX circuit
 //  that way it has negative output voltage capability
 if(gpio_export(GPIO_SCRIB_RST) ||
    gpio_set_dir(GPIO_SCRIB_RST, GPIOOUTPUT) ||
    gpio_set_value(GPIO_SCRIB_RST, 1)) {
  flukelog("ERROR: main initialization gpio export failed - GPIO_SCRIB_RST\n");
  exit(1);
 }

 // GPIO_PICSIZE controls cpld image subsampling
 //  low is fullsize 1280 x 800
 //  high is 1/3 size 427 x 266
 if(gpio_export(GPIO_PICSIZE) ||
    gpio_set_dir(GPIO_PICSIZE, GPIOOUTPUT) ||
    gpio_set_value(GPIO_PICSIZE, 0)) {
  flukelog("ERROR: main initialization gpio export failed - GPIO_PICSIZE\n");
  exit(1);
 }

 for(i=0;i<4;i++) {
  servoctrl[i].open = 0;
  servoctrl[i].resume = 0;
 }

 // Camera Defaults - sometimes (rarely) fails so retry
 //   disable PLL
 for(i=0;i<5;i++) if(!camera_i2c_write(0x5D, 0xC4)) break;
 //   clock divider - 4 is theoretical minimum and works 75% of the time
 //         5 is the lowest setting that works 100% of the time
 for(i=0;i<5;i++) if(!camera_i2c_write(0x11, 0x5)) break;
 //   set gain/exposure to fluke2 default
 for(i=0;i<5;i++) if(!camera_set_gain(0)) break;

 set_pwm(BRIGHT_LED_DEFAULT_PWM);

 flashdata_read(&scribbler_orientation, 0, 1);
 if(scribbler_orientation == 0xDF) scribbler_orientation = 0;
 else scribbler_orientation = 1;

 // parent does not wait for fork'd children to exit so ignore SIGCHLD and
 //   prevent defunct child processes
 signal(SIGCHLD, SIG_IGN);

 if(signal(SIGINT, fluke2_server_sigint) == SIG_ERR) {
  flukelog("ERROR: main initialization failed to install sighandler for SIGINT\n");
  exit(1);
 }
 interrupted = 0;

restart:

 // use first free port
 port_rfcomm = 0;
 // accept MAXCLIENTS connections
 listenfd = rfcomm_srv_sock_setup(&port_rfcomm, MAXCLIENTS);
 if(listenfd < 0) {
  flukelog("ERROR: main rfcomm_srv_sock_setup failed to create listenfd\n");
  exit(1);
 }

 sdp_session = sdp_svc_add_spp(port_rfcomm,
                "Fluke2",
                "Fluke2 Bluetooth server",
                "www.betterbots.com", 
                fluke2_rand_uuid );
 if(sdp_session == NULL) {
  flukelog("ERROR: main sdp_svc_add_spp failed to register service spp\n");
  close(listenfd);
  exit(1);
 }

 // main loop
 while(!interrupted) {

  // When the bluetooth discoverable flags are set during system startup the
  //   kernel sometimes will immediately disable them.  This function checks
  //   the flag status on every iteration of the main loop and sets the 
  //   discoverable flags if they are not already set.
  bluez_set_discoverable();

  // server listening socket accepts new connections
  nfds=1;
  fds[0].fd = listenfd;
  fds[0].events = POLLIN;

  // client sockets
  for(i=0;i<MAXCLIENTS;i++) {
   if(btclient[i].sock > 0) {
    fds[nfds].fd = btclient[i].sock;
    if(btclient[i].tx_len && btclient[i].tx_ptr)
     fds[nfds].events = POLLIN | POLLOUT;
    else fds[nfds].events = POLLIN;
    nfds++;
   }
  }

  if(camctrl.fd > 0) {
   fds[nfds].fd = camctrl.fd;
   fds[nfds].events = POLLIN;
   nfds++;
  }

  if(serctrl.fd > 0) {
   fds[nfds].fd = serctrl.fd;
   fds[nfds].events = POLLIN;
   nfds++;
  }

  // 3000 millisecond timeout
  ret = poll(fds, nfds, 3000);

  if(ret < 0) {
   if(errno != EINTR && errno != EAGAIN) {
    flukelog("ERROR: main poll failed: %s\n", strerror(errno));
    continue;
   }
  }
  else if(ret > 0) {
   for(i=0;i<nfds;i++) {
    if(fds[i].revents == 0) continue;

    // find btclient entry that matches active fds socket
    myclient = -1;
    for(j=0;j<MAXCLIENTS;j++) {
     if(btclient[j].sock == fds[i].fd) myclient=j;
    }

    // detect closed connections
    if(fds[i].revents & POLLERR || fds[i].revents == POLLHUP) {
     if(myclient >= 0) {
      flukelog("DEBUG: client disconnect (POLLHUP)\n");
      btclient[myclient].sock = 0;
     }
     else if(fds[i].fd == listenfd) {
      flukelog("ERROR: poll indicates main listenfd socket closed\n");
      goto error_restart;
     }
     else if(fds[i].fd == camctrl.fd) {
      flukelog("ERROR: poll indicates camera fd closed\n");
      camctrl.fd = 0;
     }
     else if(fds[i].fd == serctrl.fd) {
      flukelog("ERROR: poll indicates serial fd closed\n");
      camctrl.fd = 0;
     }
     continue;
    }


    // CAMERA
    else if(camctrl.fd != 0 && fds[i].fd == camctrl.fd) {
     // full image is available to be read or mmaped
     mmap_ptr = mmap(NULL, (camctrl.imagewidth * camctrl.imageheight), PROT_READ, MAP_SHARED, camctrl.fd , 0);
     if(mmap_ptr == MAP_FAILED) {
      flukelog("ERROR: camera mmap error: %s\n", strerror(errno));
      close(camctrl.fd);
      camctrl.fd = 0;
      break;
     }

     bayer_demosaic(camctrl.image, mmap_ptr, camctrl.imagewidth, camctrl.imageheight);

     if(!camctrl.colorflag) image_rgb2gray(camctrl.image);

     if(munmap(mmap_ptr, (camctrl.imagewidth * camctrl.imageheight))) {
      close(camctrl.fd);
      camctrl.fd = 0;
      flukelog("ERROR: camera munmap error: %s\n", strerror(errno));
      break;
     }

     close(camctrl.fd);
     camctrl.fd = 0;

     if(camctrl.dojpeg) {
      camctrl.jpegsize = (camctrl.imagewidth * camctrl.imageheight);   // allocated size of buffer
      camera_image_compress(camctrl.image, camctrl.jpeg, &camctrl.jpegsize, camctrl.jpegquality, camctrl.colorflag);
     }

     if(camctrl.command && camctrl.client) {
      if(fcmd[camctrl.command].process) {
       camctrl.client->caller = CALLER_CAMERA;
       camctrl.client->rx_data = camctrl.image;
       camctrl.client->command = camctrl.command;
       ret = fcmd[camctrl.command].process( camctrl.client );
       camctrl.client->command = 0;
      }
     }

     camctrl.command = 0;
     camctrl.client = NULL;

     // reset defaults
     camctrl.colorflag = 1;
     camctrl.dojpeg = 1;
     camctrl.jpegquality = JPEG_QUALITY_GOOD;

     continue;
    }


    // SERIAL
    else if(serctrl.fd != 0 && fds[i].fd == serctrl.fd) {
     // serial data available
     while(1) {
      if(!serctrl.rx_len) size = sizeof(serctrl.rx_buf) - serctrl.rx_cnt;
      else size = serctrl.rx_len - serctrl.rx_cnt;
      if(size > sizeof(serctrl.rx_buf)) {
       flukelog("ERROR: serial requested recv size (%d) exceeds rx_buf size\n", size);
       size = sizeof(serctrl.rx_buf);
      }
      ret = read(serctrl.fd, serctrl.rx_buf + serctrl.rx_cnt, size);
      // flukelog("DEBUG: serial read returned %d\n", ret);
      if(ret < 0) {
       if(errno == EAGAIN) break;
       else if(errno != EINTR) {
        flukelog("ERROR: serial read error: %s\n", strerror(errno));
        close(serctrl.fd);
        serctrl.fd = 0;
        resume_servo();
        break;
       }
      }
      else if(ret == 0) {
       if(serctrl.rx_cnt < serctrl.rx_len) {
        flukelog("ERROR: serial read end of file at only %d bytes\n", serctrl.rx_cnt);
        close(serctrl.fd);
        serctrl.fd = 0;
        resume_servo();
       }
       break;
      }
      else {
       serctrl.rx_cnt += ret;
       if(serctrl.rx_cnt >= serctrl.rx_len) break;
      }
     }

     if(serctrl.rx_cnt >= serctrl.rx_len) {
      if(serctrl.command && serctrl.client) {
       if(fcmd[serctrl.command].process) {
        serctrl.client->caller = CALLER_SERIAL;
        serctrl.client->rx_data = serctrl.rx_buf;
        serctrl.client->command = serctrl.command;
        ret = fcmd[serctrl.command].process( serctrl.client );
        serctrl.client->command = 0;
       }
      }

      close(serctrl.fd);
      serctrl.fd = 0;
      serctrl.command = 0;
      serctrl.client = NULL;
      serctrl.rx_len = 0;
      serctrl.rx_cnt = 0;

      resume_servo();

      continue;
     }
    }


    // NEW CLIENT CONNECTIONS
    else if(fds[i].fd == listenfd) {
     // find first free client
     for(j=0;j<MAXCLIENTS;j++) {
      if(btclient[j].sock <= 0) break;
     }
     if(j >= MAXCLIENTS) continue;

     if(rfcomm_srv_sock_accept(listenfd, &btclient[j]) < 0) {
      if(errno != EINTR && errno != EAGAIN) {
       flukelog("ERROR: main rfcomm_srv_sock_accept failed accepting client-connection\n");
       continue;
      }
     }

     bluez_linkkeys_store();

     flukelog("DEBUG: client connected\n");
     continue;
    }

    // RECEIVE BLUETOOTH DATA
    else if(fds[i].revents & POLLIN) {
     if(myclient < 0) continue;
     while(1) {
      if(!btclient[myclient].rx_len) 
       size = BTRXBUFSIZE - btclient[myclient].rx_end;
      else size = btclient[myclient].rx_len - btclient[myclient].rx_end +
                  btclient[myclient].rx_pos;
      if(size <= 0) {
       flukelog("ERROR: recv invalid size parameter %d\n", size);
       size = BTRXBUFSIZE - btclient[myclient].rx_end;
      }
      if(size + btclient[myclient].rx_end > BTRXBUFSIZE) {
       flukelog("ERROR: requested recv size (%d) exceeds rx_buf size\n", size);
       size = BTRXBUFSIZE - btclient[myclient].rx_end;
      }
      ret = recv(btclient[myclient].sock,
                 btclient[myclient].rx_buf + btclient[myclient].rx_end,
                 size, 0);
      /*
      flukelog("DEBUG: recv returned %d\n", ret, size);
      flukelog("DEBUG: %02X %02X %02X %02X %02X %02X %02X %02X\n", 
                btclient[myclient].rx_buf[0], btclient[myclient].rx_buf[1],
                btclient[myclient].rx_buf[2], btclient[myclient].rx_buf[3],
                btclient[myclient].rx_buf[4], btclient[myclient].rx_buf[5],
                btclient[myclient].rx_buf[6], btclient[myclient].rx_buf[7]);
      */
      if(ret < 0) {
       if(errno == EAGAIN) break;
       else if(errno == ECONNRESET) {
        flukelog("DEBUG: client disconnect (recv ECONNRESET)\n");
        btclient[myclient].sock = 0;
        break;
       }
       else if(errno != EINTR) {
        flukelog("ERROR: main recv error: %s\n", strerror(errno));
        break;
       }
      }
      else if(ret == 0) {
       flukelog("DEBUG: client disconnect (recv=0)\n");
       btclient[myclient].sock = 0;
       break;
      }
      else {
       btclient[myclient].rx_end += ret;
       if(btclient[myclient].rx_end - btclient[myclient].rx_pos >= 
          btclient[myclient].rx_len) break;
      }
     }
    }

    // TRANSMIT BLUETOOTH DATA
    else if(fds[i].revents & POLLOUT) {
     if(myclient < 0) continue;
     if(!btclient[myclient].tx_ptr || !btclient[myclient].tx_len) continue;

     while(1) {
      ret = send(btclient[myclient].sock,
                 btclient[myclient].tx_ptr + btclient[myclient].tx_cnt,
                 btclient[myclient].tx_len - btclient[myclient].tx_cnt, 0);
      // flukelog("DEBUG: send returned %d\n", ret);
      if(ret == 0) break;
      else if(ret < 0) {
       if(errno == EAGAIN) break;
       else if(errno == ECONNRESET) {
        flukelog("DEBUG: client disconnect (send ECONNRESET)\n");
        btclient[myclient].sock = 0;
        break;
       }
       else if(errno != EINTR) {
        flukelog("ERROR: main send error: %s\n", strerror(errno));
        break;
       }
      }
      else btclient[myclient].tx_cnt += ret;

      // check if transmission complete and reset counters if so
      if(btclient[myclient].tx_cnt >= btclient[myclient].tx_len) {
       btclient[myclient].tx_cnt = 0;
       btclient[myclient].tx_len = 0;
       btclient[myclient].tx_ptr = NULL;
       break;
      }
     }
    }
   }

   // EXECUTE COMMANDS
   // use the first byte received as an index into the 
   // fcmd command array defined in fluke2cmd.c
   for(i=0;i<MAXCLIENTS;i++) {

    while(btclient[i].rx_end - btclient[i].rx_pos >= btclient[i].rx_len && 
          btclient[i].rx_end - btclient[i].rx_pos > 0 ) {
     btclient[i].rx_data = btclient[i].rx_buf + btclient[i].rx_pos;

     // check if last function requested additional RX data
     if(btclient[i].command) {
      if(fcmd[btclient[i].command].process) {
       btclient[i].caller = CALLER_MAIN_REPEAT;
       ret = fcmd[btclient[i].command].process( &btclient[i] );
       btclient[i].rx_pos += btclient[i].rx_len;
       if(ret > 0) {
        // function needs even more data
        btclient[i].rx_len = ret;
       }
       else {
        // function is done
        btclient[i].command = 0;
        btclient[i].rx_len = 0;
       }
      }
      else {
       // invalid command
       btclient[i].command = 0;
       btclient[i].rx_len = 0;
       btclient[i].rx_end = 0;
       btclient[i].rx_pos = 0;
      }
     }
     else {
      cmd = btclient[i].rx_data[0];
      if(fcmd[cmd].process && fcmd[cmd].cmdlen) {
       btclient[i].rx_len = fcmd[cmd].cmdlen;
       // flukelog("DEBUG: check exec func cmd=%d rx_len=%d size=%d\n", cmd, btclient[i].rx_len, btclient[i].rx_end - btclient[i].rx_pos);

       // is there enough data to run this command?
       if(btclient[i].rx_end - btclient[i].rx_pos < btclient[i].rx_len) break;
       btclient[i].command = cmd;
       btclient[i].caller = CALLER_MAIN;
       btclient[i].rx_data++;
       // flukelog("DEBUG: exec func cmd %d cmdlen %d\n", cmd, btclient[i].rx_len);
       ret = fcmd[cmd].process( &btclient[i] );
       btclient[i].lastcommand = cmd;
       btclient[i].rx_pos += btclient[i].rx_len;
       if(ret > 0) {
        // function needs more data
        btclient[i].rx_len = ret;
       }
       else {
        btclient[i].command = 0;
        btclient[i].rx_len = 0;
       }
      }
      else {
       // invalid command
       btclient[i].rx_len = 0;
       btclient[i].rx_end = 0;
       btclient[i].rx_pos = 0;
      }
     }
     if(btclient[i].rx_pos == btclient[i].rx_end) {
      // used up every byte of incoming RX data
      btclient[i].rx_end = 0;
      btclient[i].rx_pos = 0;
     }
    }
   }

  }
  else if(ret == 0) {

   // poll timed out waiting for data

   // give up waiting for any half completed messages
   for(i=0;i<MAXCLIENTS;i++) {
    btclient[i].rx_end = 0;
    btclient[i].rx_len = 0;
    btclient[i].rx_pos = 0;
    btclient[i].command = 0;
    btclient[i].tx_cnt = 0;
    btclient[i].tx_len = 0;
    btclient[i].tx_ptr = NULL;
   }

   // do other non comms related fluke work (watchdog, etc...)

   if(camctrl.fd > 0) {
    flukelog("ERROR: main camera timeout\n");
    close(camctrl.fd);
    camctrl.fd = 0;
    camctrl.command = 0;
    camctrl.client = NULL;
   }

   if(serctrl.fd > 0) {
    close(serctrl.fd);
    serctrl.fd = 0;
    serctrl.command = 0;
    serctrl.client = NULL;
    serctrl.rx_len = 0;
    serctrl.rx_cnt = 0;
   }

   // flukelog("DEBUG: poll timed out\n");
  }
 }

 flukelog("INFO: server exiting\n");

error_restart:

 if (sdp_session) sdp_svc_del(sdp_session);

 for(j=0;j<MAXCLIENTS;j++) {
  if(btclient[j].sock > 0) {
   close(btclient[j].sock);
   btclient[j].sock = 0;
  }
 }

 if(listenfd > 0) close(listenfd);

 if(!interrupted) {
  flukelog("DEBUG: server restarting\n");
  goto restart;
 }
 else exit(0);
}
